#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include "InputParser.h"

using std::string;
using std::vector;
using std::pair;
using std::unordered_map;

class WordCounter {
	private:
	InputParser m_input_parser;
	unordered_map<string, int> m_words_hash_map;
	void countWords();

	public:

	WordCounter(const std::string& input_filename);

	/*
	 * Return the top 10 most used words with respective counters
	 */
	vector<pair<string, int>> getMostFrequentWords();
  	void printTop10Words();
};
