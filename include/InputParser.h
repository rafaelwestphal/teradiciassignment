#pragma once
#include <string>
#include <fstream>
using std::string;
using std::ifstream;

class InputParser {
	private:
	string m_input_filename;
	ifstream m_input_filestream;

	string removePonctuation(const string& cur_word);

	public:
	InputParser(const std::string& input_filename);

	/*
	 * returns the filename of the input used
	 */
	string getInputFilename();

	/*
	 * Return the next word on the input file, removing any ponctuation found
	 */
	string getNextWord();
};
