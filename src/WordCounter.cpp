#include "WordCounter.h"

#include <algorithm>
#include <fstream>
#include <vector>
#include <iostream>

using std::string;
using std::vector;
using std::pair;

struct IntCmp {
    bool operator()(const pair<string, int> &lhs, const pair<string, int> &rhs) {
        return lhs.second > rhs.second;
    }
};
WordCounter::WordCounter(const string& input_file) : m_input_parser(input_file) {
}
vector<pair<string, int>> WordCounter::getMostFrequentWords() {
	countWords();
	std::vector<pair<string, int> > answer(m_words_hash_map.begin(), m_words_hash_map.end());
	if (answer.size() >= 10) {
		std::partial_sort(answer.begin(), answer.begin() + 10, answer.end(), IntCmp());
		return vector<pair<string, int> >(answer.begin(), answer.begin() + 10);
	} else {
		std::sort(answer.begin(), answer.end(), IntCmp());
		return answer;
	}
}
void WordCounter::countWords() {
	string cur_word = m_input_parser.getNextWord();
	while(cur_word != "") {
		std::transform(cur_word.begin(), cur_word.end(), cur_word.begin(), ::tolower);
		if(m_words_hash_map.find(cur_word) == m_words_hash_map.end()) {
			m_words_hash_map[cur_word] = 0;
		}
		m_words_hash_map[cur_word]++;
		cur_word = m_input_parser.getNextWord();
	}
}

void WordCounter::printTop10Words() {
	vector<pair<string, int>> top10 = getMostFrequentWords();
	int position = 1;
	for (auto each_word : top10) {
		std::cout << "#" << position++ << " is \"" << each_word.first << "\" with " << each_word.second << " occurrence(s)" << std::endl;
	}
}

