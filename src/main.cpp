#include <iostream>
#include "WordCounter.h" 
using namespace std;

int main () {
	cout << "Type the path of the file to parse :";
	string my_path;
	cin >> my_path;
	WordCounter wc(my_path);
	wc.printTop10Words();
	return 0;
}
