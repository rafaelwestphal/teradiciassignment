#include "InputParser.h"
#include <fstream>
#include <algorithm>
using std::string;

InputParser::InputParser(const string& input_file) : m_input_filename(input_file) {
	m_input_filestream.open(m_input_filename);		
	if (!m_input_filestream) throw std::runtime_error("Could not open file to parse!");
}

string InputParser::getInputFilename() {
	return m_input_filename;
}

string InputParser::getNextWord() {
	string next_word;
	m_input_filestream >> next_word;
	if (m_input_filestream.eof()) return "";

	next_word = removePonctuation(next_word);

	while (next_word == "" && !m_input_filestream.eof()) {
		m_input_filestream >> next_word;
		next_word = removePonctuation(next_word);
	}

	return next_word;
}

string InputParser::removePonctuation(const string& cur_word) {
	string result;
	std::remove_copy_if(cur_word.begin(), cur_word.end(),
                        std::back_inserter(result), //Store output
                        std::ptr_fun<int, int>(&std::ispunct));
	return result;
}
