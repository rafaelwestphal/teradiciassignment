#include "gtest/gtest.h"
#include "WordCounter.h"
#include <string>
#include <stdexcept>
using std::string;
using std::runtime_error;

TEST(TestWordCounter, TestEmptyFile) {
	string input_text("resources/empty_file.txt");
	WordCounter wc(input_text);
	ASSERT_EQ(wc.getMostFrequentWords().size(), 0);
}

TEST(TestWordCounter, TestFileWithNumber) {
	string input_text("resources/file_with_number.txt");
	WordCounter wc(input_text);
	ASSERT_EQ(wc.getMostFrequentWords().size(), 5);
}

TEST(TestWordCounter, TestSimpleFile) {
	string input_text("resources/input1.txt");
	WordCounter wc(input_text);
	ASSERT_EQ(wc.getMostFrequentWords().size(), 10);
	vector<pair<string, int>> word_counter = wc.getMostFrequentWords();
	for (int i = 0; i < 10; i++) {
		ASSERT_EQ(word_counter[i].second, 1);
	}
}

TEST(TestWordCounter, TestBigFile) {
	string input_text("resources/big_input.txt");
	WordCounter wc(input_text);
	vector<pair<string, int>> word_counter = wc.getMostFrequentWords();
	ASSERT_EQ(word_counter[0].second, 13);
}
TEST(TestWordCounter, TestUniqueWordFile) {
	string input_text("resources/unique_word.txt");
	WordCounter wc(input_text);
	ASSERT_EQ(wc.getMostFrequentWords().size(), 1);
	ASSERT_EQ(wc.getMostFrequentWords()[0].second, 16);
}
