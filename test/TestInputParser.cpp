#include "gtest/gtest.h"
#include "InputParser.h"
#include <string>
#include <stdexcept>
using std::string;
using std::runtime_error;

TEST(TestInputParser, TestInvalidFile) {
	string input_text("resources/file_non_existant.txt");
	EXPECT_THROW(InputParser input_parser(input_text), runtime_error);
}

TEST(TestInputParser, TestFileWithNumber) {
	string input_text("resources/file_with_number.txt");
	InputParser input_parser(input_text);
	ASSERT_EQ(input_parser.getNextWord(), "h1");
	ASSERT_EQ(input_parser.getNextWord(), "teradici");
	ASSERT_EQ(input_parser.getNextWord(), "1000");
	ASSERT_EQ(input_parser.getNextWord(), "teradici");
	ASSERT_EQ(input_parser.getNextWord(), "x10");
	ASSERT_EQ(input_parser.getNextWord(), "motherinlaw");

}
TEST(TestInputParser, TestEmptyFile) {
	string input_text("resources/empty_file.txt");
	InputParser input_parser(input_text);

	ASSERT_EQ(input_parser.getInputFilename(), input_text);
	ASSERT_EQ(input_parser.getNextWord(), "");
}

TEST(TestInputParser, TestSimpleInput) {
	string input_text("resources/input1.txt");
	InputParser input_parser(input_text);

	ASSERT_EQ(input_parser.getInputFilename(), input_text);
	ASSERT_EQ(input_parser.getNextWord(), "hi");
	ASSERT_EQ(input_parser.getNextWord(), "this");
	ASSERT_EQ(input_parser.getNextWord(), "is");
	ASSERT_EQ(input_parser.getNextWord(), "a");
	ASSERT_EQ(input_parser.getNextWord(), "template");
}

TEST(TestInputParser, TestPontuactionInput) {
	string input_text("resources/ponctuation_input.txt");
	InputParser input_parser(input_text);

	ASSERT_EQ(input_parser.getInputFilename(), input_text);
	ASSERT_EQ(input_parser.getNextWord(), "trailing");
	ASSERT_EQ(input_parser.getNextWord(), "correct");
	ASSERT_EQ(input_parser.getNextWord(), "template");
	ASSERT_EQ(input_parser.getNextWord(), "");
}
